<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use Sluggable, SoftDeletes;
    public $timestamps = false;
    protected $guarded = ['id'];
    protected $appends = ['image'];


    public function getImageAttribute()
    {
        if(!empty($this->picture) && \File::exists(public_path('uploads/categories').'/'.$this->picture)){
            return asset('uploads/categories/'.$this->picture);
        }else{
            return asset('uploads/categories/default.jpg');
        }
    }


    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

}
