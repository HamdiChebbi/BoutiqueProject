<?php

namespace App\Http\Controllers\Admin;

use App\Models\Category;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = "Categories";
        $categories = Category::orderBy('name')->get();
        return view('admin.categories.index', compact('title', 'categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = "New Category";
        return view('admin.categories.create', compact('title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'picture' => 'image|max:2048'
        ]);
        $data = $request->only('name');
        if ($file = $request->file('picture')) {
            $filename = strtolower(uniqid() . '.' . $file->getClientOriginalExtension());
            $dest = public_path('uploads/categories');
            $file->move($dest, $filename);
            $data['picture'] = $filename;
        }
        Category::create($data);
        return redirect()->route('categories.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::find($id);
        if (!$category) return redirect()->route('categories.index');
        $title = "Edit Category";
        return view('admin.categories.edit', compact('category', 'title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'picture' => 'image|max:2048'
        ]);
        $category = Category::find($id);
        if (!$category) return redirect()->route('categories.index');
        $data = $request->only('name');
        if ($file = $request->file('picture')) {
            $filename = strtolower(uniqid() . '.' . $file->getClientOriginalExtension());
            $dest = public_path('uploads/categories');
            $file->move($dest, $filename);
            $data['picture'] = $filename;
            $oldFile = public_path('uploads/categories').'/'.$category->picture;
            if(!empty($category->picture) && \File::exists($oldFile)){
                @unlink($oldFile);
            }
        }
        $category->update($data);
        return redirect()->route('categories.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if ($c = Category::find($id)) {
            $c->delete();
        }
        return redirect()->route('categories.index');
    }
}
