<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::group(['prefix'=>'admin' , 'namespace'=>'Admin','middleware'=> ['auth','auth.admin']],
function($router){
  $router->get('/',['as' => 'dashboard','uses'=> 'DashboardController@index']);
    $router->resource('categories' , 'CategoriesController@index');
});
